package com.android.rootdetection;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView rootFinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rootFinder = findViewById(R.id.rootFinder);

//        executeShellCommand("su");

        RootUtil rootUtil = new RootUtil();

        if(rootUtil.isDeviceRooted()){
            rootFinder.setText("It is rooted device");
        }else {
            rootFinder.setText("It is not rooted device");
        }

    }

    private void executeShellCommand(String su) {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(su);
            rootFinder.setText("It is rooted device");
            Toast.makeText(MainActivity.this, "It is rooted device", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            rootFinder.setText("It is not rooted device");
        } finally {
            if (process != null) {
                try {
                    process.destroy();
                } catch (Exception e) { }
            }
        }
    }
}